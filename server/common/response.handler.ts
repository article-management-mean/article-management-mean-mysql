export enum ResponseStatus {
  OK = 'OK',
  ERROR = 'ERROR',
  FAILED = 'FAILED',
}

export interface IRespondReturn {
  status: ResponseStatus;
  timestamp: Date;
  [key: string]: any;
}

/**
 * Create the response message, use this to keep messages in a consistent format
 *
 * @param response
 * @param status
 */
export default (
  // eslint-disable-next-line @typescript-eslint/ban-types
  response: object,
  status: ResponseStatus = ResponseStatus.OK
): IRespondReturn => {
  const message = {
    status,
    ...response,
    timestamp: new Date(),
  };
  return message;
};
