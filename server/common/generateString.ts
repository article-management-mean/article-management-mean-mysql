import cryptoRandomString from 'crypto-random-string';

export const DEFAULT_LENGTH = 20;

export default (length = DEFAULT_LENGTH) =>
  cryptoRandomString({ length, type: 'url-safe' });
