import bcrypt from 'bcrypt';
const saltRounds = 10;

export const hash = async (password: string): Promise<string> => {
  try {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call
    const salt = await bcrypt.genSalt(saltRounds);
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call
    const encrypted = await bcrypt.hash(password, salt);
    if (encrypted) {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-return
      return encrypted;
    }
    throw new Error('Failed to hash password.');
  } catch (err) {
    // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
    throw new Error(`Error: ${err}`);
  }
};

export const verify = async (
  hashed: string,
  password: string
): Promise<boolean> => {
  try {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call
    return await bcrypt.compare(password, hashed);
  } catch (err) {
    // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
    throw new Error(`Error verify: ${err}`);
  }
};
