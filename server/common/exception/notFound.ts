import Request from './request';

export default class NotFound extends Request {
  constructor(message: string, statusCode: number = 404) {
    super(message, statusCode);
  }
}
