import knex, { Config } from 'knex';
import logger from './logger';

/**
 * Helper for creating databases with added error logging
 *
 * @param name
 * @param config
 */

export default (name: string, config: Config) =>
  knex({
    acquireConnectionTimeout: 10000,
    useNullAsDefault: true,
    ...config,
  }).on('query-error', (err) => logger.error(err));
