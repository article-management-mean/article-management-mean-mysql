import { Request, Response } from 'express';
import { RequestError } from '../common/exception';
import respond, { ResponseStatus } from '../common/response.handler';
import logger from '../common/logger';

export const GENERIC_ERROR_MESSAGE =
  'There was an error processing your request. Please try again.';

/**
 * Get the response from the exception raised
 *
 * @param error
 */
export const getResponseFromError = (error: any) => {
  if (error instanceof RequestError) {
    return {
      message: error.message,
      requestError: true,
      responseStatus: ResponseStatus.ERROR,
      statusCode: error.statusCode,
    };
  }
  // return a default message
  return {
    message: exports.GENERIC_ERROR_MESSAGE,
    requestError: false,
    responseStatus: ResponseStatus.FAILED,
    statusCode: 500,
  };
};

export const getPayload = (req: Request) => {
  // eslint-disable-next-line @typescript-eslint/no-unsafe-return
  return { ...req.body, ...req.query, ...req.params };
};

export default (route: any, key = 'data') => async (
  req: Request,
  res: Response
) => {
  try {
    // execute the route
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call
    const response = await route(getPayload(req), req);
    // no exceptions so we have a good response to send to the client
    res.status(200).json(
      respond({
        [key]: response,
      })
    );
  } catch (error) {
    // there was an error, get the response
    const {
      message,
      requestError,
      responseStatus,
      statusCode,
    } = getResponseFromError(error);
    // log the error, info for requestErrors, error for unknown errors
    requestError
      ? logger.info(error, 'request.error')
      : logger.error(error, 'request.error');
    res
      .status(statusCode)
      .json(respond({ error: [{ msg: message }] }, responseStatus));
  }
};
