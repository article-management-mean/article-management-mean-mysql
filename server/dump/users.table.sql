CREATE TABLE `article`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `firstName` VARCHAR(60) NULL,
  `lastName` VARCHAR(60) NULL,
  `email` VARCHAR(80) NULL,
  `password` VARCHAR(100) NULL,
  `confirmation` VARCHAR(45) NULL,
  `created` TIMESTAMP NULL,
  `updated` TIMESTAMP NULL,
  PRIMARY KEY (`id`));
