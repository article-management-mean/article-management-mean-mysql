CREATE TABLE `article`.`articles` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` TEXT NULL,
  `image` TEXT NULL,
  `description` LONGTEXT NULL,
  `publishDate` TIMESTAMP NULL,
  `created` TIMESTAMP NULL,
  `updated` TIMESTAMP NULL,
  PRIMARY KEY (`id`));
