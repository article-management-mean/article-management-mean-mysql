import { Application } from 'express';
import route, { baseAPI } from './common/constants';

import articlesRouter from './api/controllers/articles/articles.router';
import authRouter from './api/controllers/auth/auth.router';

export default function routes(app: Application): void {
  app.use(`${baseAPI}`, authRouter);
  app.use(`${baseAPI}${route.article}`, articlesRouter);
}
