import './common/env';
import Server from './server';
// import routes from './routes';

const port = parseInt(process.env.NODE_PORT ?? '3000');
// export default new Server().router(routes).listen(port);
export default new Server().listen(port);
