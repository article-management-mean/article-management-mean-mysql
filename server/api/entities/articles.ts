import { check } from 'express-validator';
import Model from '../config/model';
import create from '../middlewares/validate';

export interface IArticle {
  id?: number;
  title?: string;
  image?: string;
  description?: string;
  publishDate?: string;
}

export const validate = create([
  check('title').not().isEmpty().withMessage('Article title is required'),
  check('description')
    .not()
    .isEmpty()
    .withMessage('Article description is required'),
]);

export default class Articles extends Model<IArticle> {
  public getModelName = (): string => {
    return 'articles';
  };
}
