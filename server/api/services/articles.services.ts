import Articles, { IArticle } from '../entities/articles';

class ArticlesService {
  /**
   * @param id
   */
  public async getOne(id: number): Promise<IArticle | null> {
    return await new Articles().getBy({
      id,
    });
  }

  /**
   *
   */
  public async getAll(): Promise<IArticle[]> {
    return await new Articles().fetchAll();
  }

  /**
   *
   * @param data
   */
  public async add(data: IArticle): Promise<number[]> {
    return await new Articles().create(data);
  }

  /**
   *
   * @param id
   * @param branch
   */

  public async update(id: number, data: IArticle): Promise<number> {
    return await new Articles().update(
      {
        id,
      },
      data
    );
  }

  /**
   *
   * @param id
   */
  public async delete(id: number): Promise<number> {
    return await new Articles().delete({
      id,
    });
  }
}

export default new ArticlesService();
