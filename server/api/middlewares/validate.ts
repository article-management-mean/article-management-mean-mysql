import { NextFunction, Request, Response } from 'express';
import StatusCodes from 'http-status-codes';
import { ValidationChain, validationResult } from 'express-validator';
import respond, { ResponseStatus } from '../../common/response.handler';

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export default (validators: ValidationChain[]) => [
  ...validators,

  /**
   * Catch any errors and return a generic http error
   *
   * @param req
   * @param res
   * @param next
   */
  (req: Request, res: Response, next: NextFunction) => {
    const { BAD_REQUEST } = StatusCodes;
    const errors = validationResult(req);

    if (errors.isEmpty()) {
      return next();
    }

    res.status(BAD_REQUEST).json(
      respond(
        {
          error: errors.array().map((e) => {
            delete e.location;
            return e;
          }),
        },
        ResponseStatus.ERROR
      )
    );
  },
];
