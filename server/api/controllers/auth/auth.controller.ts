import { UnauthorizedError } from '../../../common/exception';
import { verify } from '../../../common/password';
import request from '../../../common/request.handler';
import { generateJWT } from '../../../common/jwt';
import Users, {
  ILoginRequest,
  IUser,
  messageUnauthorized,
  validateRegister,
  validateLogin,
} from '../../entities/users';
import generateString from '../../../common/generateString';

export const login = async ({ email, password }: ILoginRequest) => {
  const user = await new Users().getByEmail(email);

  if (!user) {
    throw new UnauthorizedError(messageUnauthorized);
  }

  if (!(await verify(user.password, password))) {
    throw new UnauthorizedError(messageUnauthorized);
  }

  const jwt = generateJWT(user.id, user.email);

  return {
    email: user.email,
    firstName: user.firstName,
    id: user.id,
    jwt,
    lastName: user.lastName,
    success: true,
  };
};

const register = async ({
  email,
  firstName,
  lastName,
  password,
}: IUser): Promise<any> => {
  try {
    const confirmation = generateString();

    await new Users().create({
      confirmation,
      email,
      firstName,
      password,
      lastName,
    });

    return {
      message: 'Registration successful',
      success: true,
    };
  } catch (error) {
    return {
      error,
      success: false,
    };
  }
};

export default {
  login: [validateLogin, request(login)],
  register: [validateRegister, request(register)],
};
