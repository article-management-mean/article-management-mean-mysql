/* eslint-disable @typescript-eslint/unbound-method */
import express from 'express';
import articles from './articles.controller';

const router = express.Router();

router.get('/', articles.getAllArticles).post('/', ...articles.createArticle);

router
  .get(`/:id`, articles.getArticleById)
  .put(`/:id`, ...articles.updateArticle)
  .delete(`/:id`, articles.deleteArticle);

router.put(`/:id/publish`, articles.publishArticle);

export default router;
