import { Request } from 'express';
import request from '../../../common/request.handler';

import { IArticle, validate } from '../../entities/articles';
import authenticated from '../../middlewares/authenticated';
import ArticlesService from '../../services/articles.services';

export interface IArticleParams {
  id: number;
}

export const getAllArticles = async () => {
  try {
    const articles = await ArticlesService.getAll();
    return {
      articles,
      success: true,
    };
  } catch (error) {
    return {
      error,
      success: false,
    };
  }
};

const getArticleById = async ({ id }: IArticleParams) => {
  try {
    const article = await ArticlesService.getOne(id);
    return {
      article,
      success: true,
    };
  } catch (error) {
    return {
      error,
      success: false,
    };
  }
};

const createArticle = async (data: IArticle) => {
  try {
    const article = await ArticlesService.add(data);
    return {
      article: article && article.length > 0 ? article[0] : {},
      success: true,
    };
  } catch (error) {
    return {
      error,
      success: false,
    };
  }
};

const updateArticle = async ({ id }: IArticleParams, req: Request) => {
  try {
    const article = await ArticlesService.update(id, req.body);
    return {
      article,
      success: true,
    };
  } catch (error) {
    return {
      error,
      success: false,
    };
  }
};

const deleteArticle = async ({ id }: IArticleParams) => {
  try {
    const article = await ArticlesService.delete(id);
    return {
      article,
      success: true,
    };
  } catch (error) {
    return {
      error,
      success: false,
    };
  }
};

const publishArticle = async ({ id }: IArticleParams) => {
  try {
    const article = await ArticlesService.update(id, {
      publishDate: new Date().toISOString(),
    });
    return {
      article,
      success: true,
    };
  } catch (error) {
    return {
      error,
      success: false,
    };
  }
};

export default {
  getAllArticles: [authenticated, request(getAllArticles)],
  createArticle: [authenticated, validate, request(createArticle)],
  getArticleById: [authenticated, request(getArticleById)],
  updateArticle: [authenticated, validate, request(updateArticle)],
  deleteArticle: [authenticated, request(deleteArticle)],
  publishArticle: [authenticated, request(publishArticle)],
};
