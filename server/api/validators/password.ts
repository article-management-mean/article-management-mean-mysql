import { PasswordMin } from '../../common/constants';

export const messagePasswordEmpty = 'Password cannot be empty';

export const messagePasswordTooShort = `Password must be at least ${PasswordMin} characters long`;

export const messagePasswordTooSimple = `Password must contain lowercase characters`;

/**
 * Validate the password
 *
 * @param password
 */
export default (password?: string): boolean => {
  if (typeof password !== 'string') {
    throw new Error(messagePasswordEmpty);
  }

  if (password.length < PasswordMin) {
    // doesn't meet the minimum length
    throw new Error(messagePasswordTooShort);
  }

  if (password.search(/[a-z]/) === -1) {
    // no lower case letters
    throw new Error(messagePasswordTooSimple);
  }

  return true;
};
