import { QueryBuilder } from 'knex';
import db from './db';

/**
 * Model with dates interface
 */
export interface IModelWithDates {
  DateCreated: Date;
  DateDeleted?: Date;
  DateModified: Date;
}

/**
 * Types of column statuses for attempts
 */
export enum StatusAttempting {
  Failure = 'failure',
  Successful = 'successful',
}

/**
 * Types of column statuses for pending/confirming
 */
export enum StatusCreating {
  Pending = 'pending',
  Confirmed = 'confirmed',
  Cancelled = 'cancelled',
}

/**
 * Types of column statuses for enabling/disabling
 */
export enum StatusEnabling {
  Enabled = 'enabled',
  Disabled = 'disabled',
  Suspended = 'suspended',
  Deleted = 'deleted',
}

/**
 * Base model class for interacting with the data store
 */
// eslint-disable-next-line @typescript-eslint/no-unused-vars
export default abstract class Model<T, _K = number> {
  /**
   * Get the model name which is also used as the table name
   */
  public abstract getModelName(): string;

  /**
   * Save the model to the store
   *
   * @param model
   */
  public create = async (model: Partial<T>): Promise<number[]> =>
    this.query().insert(
      await this.beforeSet({
        created: new Date(),
        updated: new Date(),
        ...model,
      })
    );

  /**
   * Perform a simple fetch on the model and return the results
   */
  public fetch = async (search: Partial<T>): Promise<T[]> =>
    this.query()
      .where(search)
      .then((rows: T[]): T[] => rows);

  /**
   * Fetch all of the rows from the table
   */
  public fetchAll = async (): Promise<T[]> =>
    this.query().then((rows: T[]): T[] => rows);

  /**
   * Perform a primary key lookup on the model and return a single result
   *
   * @param id
   */
  public get = async (id: number): Promise<T> => this.getBy({ id });

  /**
   * Get a model by the where
   *
   * @param where
   */
  public getBy = async (where: any): Promise<T> =>
    this.query().where(where).first();

  /**
   * Perform a query on the dataset
   */
  public query = (): QueryBuilder => db(this.getModelName());

  /**
   * Update the model
   *
   * @param where
   * @param model
   */
  public update = async (where: Partial<T>, model: Partial<T>) =>
    this.query()
      .where(where)
      .update(
        await this.beforeSet({
          updated: new Date(),
          ...model,
        })
      );

  /**
   * Delete the model
   *
   * @param where
   */
  public delete = async (where: Partial<T>) =>
    this.query().where(where).delete();

  /**
   * Override to allow fields to be changed before updating a model
   * and saving to the database
   *
   * @param model
   */
  // eslint-disable-next-line @typescript-eslint/require-await
  protected beforeSet = async (model: Partial<T>) => model;
}
