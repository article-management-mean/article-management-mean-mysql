import env from './env';
import knex from 'knex';
import { IDBConfig } from '../../common/types';

const config: IDBConfig = {
  db: {
    database: env('DB_DATABASE'),
    host: env('DB_HOST'),
    password: env('DB_PASSWORD'),
    port: +env('DB_PORT'),
    user: env('DB_USER'),
  },
};

export default knex({
  client: env('DB_ADAPTER'),
  connection: () => config.db,
});
