import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root',
})
export class ToastService {
  constructor() {}

  showError(header: string, message: string) {
    return Swal.fire({
      icon: 'error',
      title: header,
      text: message,
    });
  }

  showInfo(header: string, message: string) {
    return Swal.fire({
      icon: 'info',
      title: header,
      text: message,
    });
  }

  showQuestion(header: string, message: string) {
    return Swal.fire({
      icon: 'question',
      title: header,
      text: message,
    });
  }

  showSuccess(header: string, message: string) {
    return Swal.fire({
      icon: 'success',
      title: header,
      text: message,
    });
  }

  showWarning(header: string, message: string) {
    return Swal.fire({
      icon: 'warning',
      title: header,
      text: message,
    });
  }
}
