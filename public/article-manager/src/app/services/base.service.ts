import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import {
  CURRENT_USER,
  CURRENT_USER_TOKEN,
} from '../shared/constants/constants';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class BaseService {
  constructor(private http: HttpClient) {}

  protected getAPIBase(route: string = ''): string {
    return `${environment.baseUrl}${environment.apiVersion}${route}`;
  }

  private getToken(): string | null {
    const token = localStorage.getItem(CURRENT_USER_TOKEN);
    return !!token ? JSON.parse(token) : null;
  }

  protected commonStateChangeHeaders(): HttpHeaders {
    return new HttpHeaders({
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: `Bearer ${this.getToken()}`,
    });
  }

  protected get(route: string): Observable<any> {
    const url = this.getAPIBase(route);
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.getToken()}`,
      Accept: 'application/json',
    });
    const options = { headers };
    return this.http.get(url, options);
  }

  protected getParams(route: string, params: any): Observable<any> {
    const url = this.getAPIBase(route);
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.getToken()}`,
      Accept: 'application/json',
    });
    const options = { headers, params };
    return this.http.get(url, options);
  }

  protected post(route: string, object?: any): Observable<any> {
    return this.http.post(this.getAPIBase(route), object, {
      headers: this.commonStateChangeHeaders(),
    });
  }

  protected postToParams(
    API: string,
    route: string,
    params: any
  ): Observable<any> {
    return this.http.post(
      this.getAPIBase(route),
      {},
      { headers: this.commonStateChangeHeaders(), params }
    );
  }

  protected upload(route: string, object: any): Observable<any> {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.getToken()}`,
      Accept: 'application/json',
    });
    headers.set('Content-Type', 'multipart/form-data');

    return this.http.post(this.getAPIBase(route), object, {
      headers,
      reportProgress: true,
      observe: 'events',
    });
  }

  protected delete(route: string, object?: any): Observable<any> {
    return this.http.delete(this.getAPIBase(route), {
      headers: this.commonStateChangeHeaders(),
      ...object,
    });
  }

  protected put(route: string, object: any): Observable<any> {
    return this.http.put(this.getAPIBase(route), object, {
      headers: this.commonStateChangeHeaders(),
    });
  }

  protected patch(route: string, object: any): Observable<any> {
    return this.http.patch(this.getAPIBase(route), object, {
      headers: this.commonStateChangeHeaders(),
    });
  }

  public handleError = (error: any) => {};
}
