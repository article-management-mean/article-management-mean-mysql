import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class LocalStorageService {
  setItem(key: string, value: any) {
    localStorage.setItem(key, JSON.stringify(value));
  }

  removeItem(item: string) {
    localStorage.removeItem(item);
  }

  getItem(item: string) {
    const data = localStorage.getItem(item);
    return !!data ? JSON.parse(data) : null;
  }

  clearStorage() {
    return localStorage.clear();
  }
}
