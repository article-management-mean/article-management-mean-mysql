import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { BaseService } from './base.service';
import { API_ROUTES } from '../shared/constants/constants';
import { IArticle } from '../models/articles';
import { ILogin, IRegister } from '../models/auth';

@Injectable({
  providedIn: 'root',
})
export class MainService extends BaseService {
  constructor(http: HttpClient) {
    super(http);
  }

  private queryString(params: any) {
    return Object.keys(params)
      .map((key) => key + '=' + params[key])
      .join('&');
  }

  login(data: ILogin): Observable<any> {
    return this.post(`${API_ROUTES.auth}`, data);
  }

  register(data: IRegister): Observable<any> {
    return this.post(`${API_ROUTES.register}`, data);
  }

  getAllArticles(): Observable<any> {
    return this.get(`${API_ROUTES.article}`);
  }

  createArticle(data: IArticle): Observable<any> {
    return this.post(`${API_ROUTES.article}`, data);
  }

  getArticle(id: number): Observable<any> {
    return this.get(`${API_ROUTES.article}/${id}`);
  }

  deleteArticle(id: number): Observable<any> {
    return this.delete(`${API_ROUTES.article}/${id}`);
  }

  updateArticle(id: number, data: IArticle): Observable<any> {
    return this.put(`${API_ROUTES.article}/${id}`, data);
  }

  publishArticle(id: number): Observable<any> {
    return this.put(`${API_ROUTES.article}/${id}/publish`, null);
  }
}
