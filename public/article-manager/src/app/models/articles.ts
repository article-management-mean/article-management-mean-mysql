export interface IArticle {
  id?: number;
  title?: string;
  image?: string;
  description?: string;
  publishDate?: string;
}

export interface IArticleExtended extends IArticle {
  created?: string;
  updated?: string;
}


export interface IArticleResponse {
  status?: string;
  data: {
    article?: number | IArticle;
    articles?: IArticle[];
    success?: boolean;
  };
  timestamp?: string;
}
