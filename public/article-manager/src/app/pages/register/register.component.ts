import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { MainService } from 'src/app/services/main.service';
import { ToastService } from 'src/app/services/toast.service';
import { IRegister } from 'src/app/models/auth';
import { emailValidationRegex } from './../../shared/constants/constants';

@Component({
  selector: 'app-register',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  isSubmitting = false;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private mainService: MainService,
    private toastService: ToastService
  ) {}

  ngOnInit(): void {
    this.createForm();
  }

  get f(): any {
    return this.registerForm.controls;
  }

  register() {
    this.isSubmitting = true;

    if (this.registerForm.invalid) {
      this.isSubmitting = false;
      this.validateAllFormFields(this.registerForm);
      return;
    }

    const payload: IRegister = {
      email: this.registerForm.value.email,
      password: this.registerForm.value.password,
      confirmPassword: this.registerForm.value.confirmPassword,
      firstName: this.registerForm.value.firstName,
      lastName: this.registerForm.value.lastName,
    };

    this.mainService
      .register(payload)
      .toPromise()
      .then((response) => {
        if (response) {
          this.isSubmitting = false;
          // this.router.navigate(['/login']);
        }
      })
      .catch((err) => {
        console.log('err: ', err);
        this.isSubmitting = false;
        if (err) {
          const error = err.error;
          console.log('error: ', error);
          if (error && error.message) {
            return this.toastService.showError('Warning!', error.message);
          }
        }
      });
  }

  private createForm() {
    this.registerForm = this.formBuilder.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      email: [
        '',
        [Validators.required, Validators.pattern(emailValidationRegex)],
      ],
      password: ['', [Validators.required, Validators.minLength(5)]],
      confirmPassword: [
        '',
        [
          Validators.required,
          Validators.minLength(5),
          this.checkPasswords.bind(this),
        ],
      ],
    });
    this.registerForm.reset();
  }

  private validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  private checkPasswords(control: FormControl): { [s: string]: boolean } {
    if (this.registerForm) {
      if (control.value !== this.registerForm.controls.password.value) {
        return { passwordNotMatch: true };
      }
    }
    return {
      passwordNotMatch: false,
    };
  }
}
