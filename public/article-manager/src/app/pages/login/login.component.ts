import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';
import { ILogin } from 'src/app/models/auth';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { MainService } from 'src/app/services/main.service';
import { ToastService } from 'src/app/services/toast.service';
import {
  CURRENT_USER,
  CURRENT_USER_TOKEN,
  emailValidationRegex,
} from './../../shared/constants/constants';

@Component({
  selector: 'app-login',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  isSubmitting = false;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private mainService: MainService,
    private toastService: ToastService,
    private storage: LocalStorageService
  ) {}

  ngOnInit(): void {
    this.createForm();
  }

  get f(): any {
    return this.loginForm.controls;
  }

  showPassword(event: any): void {
    if (event.type === 'text') {
      event.type = 'password';
    } else {
      event.type = 'text';
    }
  }

  login() {
    this.isSubmitting = true;

    if (this.loginForm.invalid) {
      this.isSubmitting = false;
      this.validateAllFormFields(this.loginForm);
      return;
    }

    const payload: ILogin = {
      email: this.loginForm.value.email,
      password: this.loginForm.value.password,
    };

    this.mainService
      .login(payload)
      .toPromise()
      .then((response) => {
        if (response && response.status === 'OK') {
          this.storage.setItem(CURRENT_USER, response.data);
          this.storage.setItem(CURRENT_USER_TOKEN, response.data.jwt);

          this.isSubmitting = false;
          this.router.navigate(['/main/dashboard']);
        }
      })
      .catch((err) => {
        console.log('err: ', err);
        this.isSubmitting = false;
        if (err) {
          const error = err.error;
          console.log('error: ', error);
          if (error && error.message) {
            return this.toastService.showError('Warning!', error.message);
          } else if (error && error.error) {
            error.error.forEach((item: any) => {
              return this.toastService.showError('Warning!', item.msg);
            });
          }
        }
      });
  }

  private createForm() {
    this.loginForm = this.formBuilder.group({
      email: [
        '',
        [Validators.required, Validators.pattern(emailValidationRegex)],
      ],
      password: ['', [Validators.required, Validators.minLength(5)]],
    });
    this.loginForm.reset();
  }

  private validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
}
