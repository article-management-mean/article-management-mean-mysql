import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import {
  IArticle,
  IArticleExtended,
  IArticleResponse,
} from 'src/app/models/articles';
import { MainService } from 'src/app/services/main.service';
import { FORMSTATE } from 'src/app/shared/constants/constants';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.scss'],
})
export class ArticlesComponent implements OnInit, OnDestroy {
  private unsubscribe = new Subject<any>();
  articlesArr: IArticleExtended[] = [];

  constructor(private mainService: MainService, private router: Router) {}

  ngOnInit(): void {
    this.initArticles();
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  onNewArticle() {
    this.router.navigate(['/main/articles/article-entry'], {
      queryParams: {
        action: FORMSTATE.ADD,
      },
    });
  }

  onUpdateArticle(data: IArticle) {
    this.router.navigate(['/main/articles/article-entry'], {
      queryParams: {
        action: FORMSTATE.UPDATE,
        id: data.id,
      },
    });
  }

  onPublishArticle(data: IArticle) {
    const onPublishArticle = () => {
      this.mainService
        .publishArticle(Number(data.id))
        .pipe(take(1))
        .toPromise()
        .then((resp: any) => {
          if (resp && resp.status === 'OK') {
            this.onRefreshArticles();

            Swal.fire('Published!', '', 'success');
          }
        });
    };

    Swal.fire({
      title: 'Do you want to publish the article?',
      showCancelButton: true,
      confirmButtonText: `Confirm`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        onPublishArticle();
      }
    });
  }

  onDeleteArticle(data: IArticle) {
    const onDeleteArticle = () => {
      this.mainService
        .deleteArticle(Number(data.id))
        .pipe(take(1))
        .toPromise()
        .then((resp: any) => {
          if (resp && resp.status === 'OK') {
            this.onRefreshArticles();

            Swal.fire('Deleted!', '', 'success');
          }
        });
    };

    Swal.fire({
      title: 'Do you want to delete this article?',
      showCancelButton: true,
      confirmButtonText: `Confirm`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        onDeleteArticle();
      }
    });
  }

  onRefreshArticles() {
    this.articlesArr = [];
    this.initArticles();
  }

  private initArticles() {
    this.mainService
      .getAllArticles()
      .pipe(takeUntil(this.unsubscribe))
      .toPromise()
      .then((resp: IArticleResponse) => {
        if (resp && resp.status === 'OK') {
          if (resp.data.articles) {
            this.articlesArr = [...resp.data.articles];
          }
          console.log('this.articlesArr: ', this.articlesArr);
        }
      });
  }
}
