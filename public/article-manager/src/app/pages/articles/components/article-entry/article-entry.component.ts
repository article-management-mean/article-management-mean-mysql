import { Location } from '@angular/common';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { Subject, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { IArticle, IArticleResponse } from 'src/app/models/articles';

import { MainService } from 'src/app/services/main.service';
import { ToastService } from 'src/app/services/toast.service';
import { FORMSTATE } from 'src/app/shared/constants/constants';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-article-entry',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './article-entry.component.html',
  styleUrls: ['./article-entry.component.scss'],
})
export class ArticleEntryComponent implements OnInit {
  articleForm: FormGroup;
  htmlContent: string;
  title: string;
  action: string;
  articleId: number;

  showPublish: boolean = false;

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: 'auto',
    minHeight: '0',
    maxHeight: 'auto',
    width: 'auto',
    minWidth: '0',
    translate: 'yes',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter text here...',
    defaultParagraphSeparator: '',
    defaultFontName: '',
    defaultFontSize: '',
    fonts: [
      { class: 'arial', name: 'Arial' },
      { class: 'times-new-roman', name: 'Times New Roman' },
      { class: 'calibri', name: 'Calibri' },
      { class: 'comic-sans-ms', name: 'Comic Sans MS' },
    ],
    customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText',
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    uploadUrl: 'v1/image',
    uploadWithCredentials: false,
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [['bold', 'italic'], ['fontSize']],
  };

  private unsubscribe = new Subject<any>();

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private formBuilder: FormBuilder,
    private toastService: ToastService,
    private mainService: MainService
  ) {
    this.route.queryParams.pipe(take(1)).subscribe((params) => {
      console.log('params: ', params);
      if (params && params.action) {
        this.action = params.action;
      }

      if (params && params.id) {
        this.articleId = +params.id;

        if (this.action === FORMSTATE.UPDATE) {
          this.getArticle(this.articleId);
        }
      }
    });
  }

  ngOnInit(): void {}

  onGoBack() {
    this.location.back();
  }

  saveArticle() {
    const saveArticle = () => {
      this.mainService
        .createArticle({
          description: this.htmlContent,
          title: this.title,
        })
        .pipe(take(1))
        .toPromise()
        .then((resp: any) => {
          if (resp && resp.status === 'OK') {
            Swal.fire('Saved!', '', 'success').then(() => {
              this.onGoBack();
            });
          }
        });
    };

    const updateArticle = () => {
      this.mainService
        .updateArticle(this.articleId, {
          description: this.htmlContent,
          title: this.title,
        })
        .pipe(take(1))
        .toPromise()
        .then((resp: any) => {
          if (resp && resp.status === 'OK') {
            Swal.fire('Updated!', '', 'success').then(() => {
              this.onGoBack();
            });
          }
        });
    };

    Swal.fire({
      title: 'Do you want to save the changes?',
      showCancelButton: true,
      confirmButtonText: `Save`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        if (this.action === FORMSTATE.ADD) {
          saveArticle();
        } else if (this.action === FORMSTATE.UPDATE) {
          updateArticle();
        }
      }
    });
  }

  publishArticle() {
    const onPublishArticle = () => {
      this.mainService
        .publishArticle(this.articleId)
        .pipe(take(1))
        .toPromise()
        .then((resp: any) => {
          if (resp && resp.status === 'OK') {
            Swal.fire('Published!', '', 'success');
          }
        });
    };

    Swal.fire({
      title: 'Do you want to publish the article?',
      showCancelButton: true,
      confirmButtonText: `Confirm`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        onPublishArticle();
      }
    });
  }

  private getArticle(id: number) {
    let subscription = new Subscription();
    subscription = this.mainService.getArticle(id).subscribe(
      (resp: IArticleResponse) => {
        if (resp && resp.status === 'OK') {
          const data = resp.data;
          const article = data.article as IArticle;
          console.log('data: ', data);
          if (article) {
            this.title = String(article.title);
            this.htmlContent = String(article.description);
          }
          this.showPublish = true;
          subscription.unsubscribe();
        }
      },
      () => {
        this.showPublish = false;
        subscription.unsubscribe();
      }
    );
  }
}
