import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularEditorModule } from '@kolkov/angular-editor';

import { ArticleEntryRoutingModule } from './article-entry-routing.module';
import { ArticleEntryComponent } from './article-entry.component';
import { ComponentsModule } from 'src/app/shared/components/components.module';

@NgModule({
  declarations: [ArticleEntryComponent],
  imports: [
    CommonModule,
    ArticleEntryRoutingModule,
    ComponentsModule,
    ReactiveFormsModule,
    AngularEditorModule,
    FormsModule,
  ],
})
export class ArticleEntryModule {}
