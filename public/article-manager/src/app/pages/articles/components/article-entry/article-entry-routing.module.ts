import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ArticleEntryComponent } from './article-entry.component';

const routes: Routes = [{ path: '', component: ArticleEntryComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArticleEntryRoutingModule { }
