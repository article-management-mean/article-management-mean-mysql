export const CURRENT_USER = 'app_user';
export const CURRENT_USER_TOKEN = 'app_current_user_token';
export const CURRENT_USER_PROFILE = 'app_current_user_profile';

export const API_ROUTES = {
  article: `/articles`,
  auth: `/login`,
  register: `/register`,
};

export const FORMSTATE = {
  ADD: 'add',
  UPDATE: 'update',
  DELETE: 'delete',
  READ: 'read',
};


export const emailValidationRegex =
  '[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}';
export const emailRegex = '[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}';
export const passwordValidationRegex =
  '^((?=.*[0-9])|(?=.*[!@#$%^&*]))[a-zA-Z0-9!@#$%^&*-_+=()]{8,50}$';