import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardSidebarHeadingComponent } from './dashboard-sidebar-heading.component';

describe('DashboardSidebarHeadingComponent', () => {
  let component: DashboardSidebarHeadingComponent;
  let fixture: ComponentFixture<DashboardSidebarHeadingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardSidebarHeadingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardSidebarHeadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
