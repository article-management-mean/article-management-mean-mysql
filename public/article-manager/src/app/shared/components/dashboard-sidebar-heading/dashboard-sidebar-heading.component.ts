import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-dashboard-sidebar-heading',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './dashboard-sidebar-heading.component.html',
  styleUrls: ['./dashboard-sidebar-heading.component.scss'],
})
export class DashboardSidebarHeadingComponent implements OnInit {
  @Input() title: string | undefined;
  @Input() icon: string | undefined;

  constructor() {}

  ngOnInit(): void {}
}
