import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from 'src/app/services/local-storage.service';

@Component({
  selector: 'app-main-header',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './main-header.component.html',
  styleUrls: ['./main-header.component.scss'],
})
export class MainHeaderComponent implements OnInit {
  constructor(private router: Router, private storage: LocalStorageService) {}

  ngOnInit(): void {}

  logout() {
    this.storage.clearStorage();
    setTimeout(() => {
      this.router.navigate(['/login']);
    }, 600);
  }
}
