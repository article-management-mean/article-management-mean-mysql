import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-nav-item',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './nav-item.component.html',
  styleUrls: ['./nav-item.component.scss'],
})
export class NavItemComponent implements OnInit {
  @Input() title: string | undefined;
  @Input() icon: string | undefined;
  @Input() active: boolean| undefined;
  @Input() link: string | undefined;

  constructor() {}

  ngOnInit(): void {}
}
