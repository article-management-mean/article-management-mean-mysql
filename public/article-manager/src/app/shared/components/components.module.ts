import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { DashboardSidebarComponent } from './dashboard-sidebar/dashboard-sidebar.component';
import { MainHeaderComponent } from './main-header/main-header.component';
import { PageToolbarComponent } from './page-toolbar/page-toolbar.component';
import { NavItemComponent } from './nav-item/nav-item.component';
import { DashboardSidebarHeadingComponent } from './dashboard-sidebar-heading/dashboard-sidebar-heading.component';


const COMPONENTS = [
  DashboardSidebarComponent,
  DashboardSidebarHeadingComponent,
  MainHeaderComponent,
  PageToolbarComponent,
  NavItemComponent,
];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [CommonModule,RouterModule],
  exports: [...COMPONENTS],
})
export class ComponentsModule {}
