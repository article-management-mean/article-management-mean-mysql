import { Location } from '@angular/common';
import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewEncapsulation,
} from '@angular/core';

@Component({
  selector: 'app-page-toolbar',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './page-toolbar.component.html',
  styleUrls: ['./page-toolbar.component.scss'],
})
export class PageToolbarComponent implements OnInit {
  @Input() title: string | undefined;
  @Input() showAddButton: boolean | undefined;
  @Input() showRefreshButton: boolean | undefined;
  @Input() showBackButton: boolean | undefined;

  @Output() onNewEvent = new EventEmitter<any>();
  @Output() onRefreshEvent = new EventEmitter<any>();
  @Output() onBackEvent = new EventEmitter<any>();

  constructor(private location: Location) {}

  ngOnInit(): void {}

  onGoingBack(event: any) {
    console.log('onBackEvent: ', this.onBackEvent.observers);
    if (this.onBackEvent.observers && this.onBackEvent.observers.length > 0) {
      this.onBackEvent.emit(event);
    } else {
      this.location.back();
    }
  }
}
